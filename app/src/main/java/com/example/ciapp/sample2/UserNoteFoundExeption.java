package com.example.ciapp.sample2;

public class UserNoteFoundExeption extends Exception {
    public UserNoteFoundExeption(String msg){
        super(msg);
    }
}
